// SocialIcons selection of Social icons that link to user profile
class IconData {
  String _classFormat;
  String _icon;
  String _title;
  int _height;
  int _width;
  IconData.data(this._classFormat, this._icon, this._height, this._width, [this._title]);
  IconData();
  factory IconData.fromJson(Map<String, dynamic> json) => _iconDataFromJson(json);

  //classFormat css classes
  String get classFormat => this._classFormat;
  set classFormat(String classFormat) {
    this._classFormat = classFormat;
  }

  //icon The icon location of the sprite
  String get icon => this._icon;
  set icon(String icon) {
    this._icon = icon;
  }

  //title Button Label
  String get title => this._title;
  set title(String title) {
    this._title = title;
  }

  //height Size of icon height
  int get height => this._height;
  set height(int height) {
    this._height = height;
  }

  //width Size of icon width
  int get width => this._width;
  set width(int width) {
    this._width = width;
  }
}

IconData _iconDataFromJson(Map<String, dynamic> json) {
  IconData x = new IconData();
  x.classFormat = json['classFormat'] as String;
  x.icon = json['icon'] as String;
  x.title = json['title'] as String;
  x.height = json['height'] as int;
  x.width = json['width'] as int;
  return x;
}

class IconLogo extends IconData {
  String _abr;
  IconLogo();
  IconLogo.data(this._abr);
  factory IconLogo.fromJson(Map<String, dynamic> json) => _iconLogoFromJson(json);
  //abr
  String get abr => this._abr;
  set abr(String abr) {
    this._abr = abr;
  }
}

IconLogo _iconLogoFromJson(Map<String, dynamic> json) {
  IconLogo x = new IconLogo();
  x.classFormat = json['classFormat'] as String;
  x.icon = json['icon'] as String;
  x.title = json['title'] as String;
  x.height = json['height'] as int;
  x.width = json['width'] as int;
  x._abr = json['abr'];
  return x;
}
