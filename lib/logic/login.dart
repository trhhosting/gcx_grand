import 'package:angular/core.dart';

// Base Login Class
class _Login {
  int _id;
  String _userName;
  String _password;
  String _apiAccessUrl;
  //id Amount of Requests to login Api
  int get id => this._id;
  set id(int id) {
    this._id = id;
  }

  //userName Auth Username
  String get userName => this._userName;
  set userName(String userName) {
    this._userName = userName;
  }

  //password User password Hash to compare with the API
  String get password => this._password;
  set password(String password) {
    this._password = password;
  }

  //apiAccessUrl URI for the access API
  String get apiAccessUrl => this._apiAccessUrl;
  set apiAccessUrl(String apiAccessUrl) {
    this._apiAccessUrl = apiAccessUrl;
  }
}

@Injectable()
class UserLogin extends _Login {}
