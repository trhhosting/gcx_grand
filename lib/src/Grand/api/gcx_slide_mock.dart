import 'dart:math';

import 'package:app/logic/button.dart';
import 'package:app/logic/event.dart';

Random num = new Random();

List<MainSlideData> loadGcxMainSlide([int howMany = 1]) {
  List<MainSlideData> data = <MainSlideData>[];
  Random num = new Random();

  for (int i = 0; i < howMany; i++) {
    MainSlideData x = new MainSlideData()
      ..id = i
      ..title = _titles[num.nextInt(3)]
      ..subTitle = _subTitles[num.nextInt(3)]
      ..callToAction = _callToActions[num.nextInt(3)]
      ..carousels = _carousels;
    data.add(x);
  }
  return data;
}

List<imageCarousel> _carousels = [
  imageCarousel.data(_titles[0], _subTitles[0], "/assets/img/gcx_slides/photo-of-people-laaning-on-wooden-table-3184638.jpg", 1),
  imageCarousel.data(_titles[1], _subTitles[1], "/assets/img/gcx_slides/photo-of-men-doing-fist-bump-3184302.jpg", 2),
  imageCarousel.data(_titles[2], _subTitles[2], "/assets/img/gcx_slides/photo-of-people-near-computers-3183159.jpg", 3),
  imageCarousel.data(_titles[3], _subTitles[3], "/assets/img/gcx_slides/Slide-03.jpg", 4),
];

// Default Inputs
/*List<Map<String, String>>  = [
  {"buttonText": "Get More", "buttonLink": "#"},
  {"buttonText": "Order Now", "buttonLink": "#"},
  {"buttonText": "Search", "buttonLink": "#"},
  {"buttonText": "Read More", "buttonLink": "#"},
];*/
List<Button> _callToActions = [
  Button.data("/#/home", "animated fadeInUp wow btn btn-lg gcx-btn-action", "Get Now"),
  Button.data("/#/home", "animated fadeInUp wow btn btn-lg gcx-btn-action", "Buy Now"),
  Button.data("/#/home", "animated fadeInUp wow btn btn-lg gcx-btn-action", "Start Now"),
  Button.data("/#/home", "animated fadeInUp wow btn btn-lg gcx-btn-action", "Don't wait"),
];
//List<video> _videos = [
//  video.data(0, {"mp4": "/static/video/lady.mp4", "webm": "/static/video/lady.webm"}, "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg"),
//  video.data(1, {"mp4": "/static/video/man.mp4", "webm": "/static/video/man.webm"}, "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg"),
//];
//Titles varchar(50)
List<String> _titles = [
"wasting time energy or effort on",
"marketing campaigns ",
"setting up lead generations systems ",
"ads that might work",
];
//Sub Titles varchar(200)
List<String> _subTitles = [
"your gym marketing ideas, lead generation or conversion again",
"for your gym marketing, lead generation or conversion again",
"for your gym marketing, lead generation or conversion again",
"for your gym marketing ideas, lead generation or conversion again",
];
