import 'package:angular/angular.dart';

import '../../shared/About/about.dart';
import '../../shared/Blog/blog.dart';
import '../../shared/CountdownTimer/countdown_timer.dart';
import '../../shared/CounterSection/counter_section.dart';
import '../../shared/EventSlides/event_slides.dart';
import '../../shared/Faq/faq.dart';
import '../../shared/Gallery/gallery.dart';
import '../../shared/MainSlide/main_slide.dart';
import '../../shared/Pricing/pricing.dart';
import '../../shared/Schedules/schedules.dart';
import '../../shared/Services/services.dart';
import '../../shared/Sponsors/sponsors.dart';
import '../../shared/Subscribe/subscribe.dart';
import '../../shared/Team/team.dart';

@Component(
  selector: 'landing',
  templateUrl: 'landing.html',
  directives: [
    coreDirectives,
    CountdownTimer,
    Services,
    About,
    CounterSection,
    Schedules,
    Team,
    Gallery,
    Faq,
    Sponsors,
    Pricing,
    EventSlides,
    Blog,
    Subscribe,
    MainSlide,
  ],
)
class Landing {}
