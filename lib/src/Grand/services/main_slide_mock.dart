import 'dart:math';

import 'package:app/logic/button.dart';
import 'package:app/logic/event.dart';

Random num = new Random();

List<MainSlideData> loadHeaderMainSlide([int howMany = 1]) {
  List<MainSlideData> data = <MainSlideData>[];
  Random num = new Random();

  for (int i = 0; i < howMany; i++) {
    MainSlideData x = new MainSlideData()
      ..id = i
      ..title = _titles[num.nextInt(15)]
      ..subTitle = _subTitles[num.nextInt(8)]
      ..callToAction = _callToActions[num.nextInt(3)]
      ..carousels = _carousels;
    data.add(x);
  }
  return data;
}

List<imageCarousel> _carousels = [
  imageCarousel.data(_titles[num.nextInt(15)], _subTitles[num.nextInt(8)], "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg", 1),
  imageCarousel.data(_titles[num.nextInt(15)], _subTitles[num.nextInt(8)], "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg", 2),
  imageCarousel.data(_titles[num.nextInt(15)], _subTitles[num.nextInt(8)], "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg", 3),
  imageCarousel.data(_titles[num.nextInt(15)], _subTitles[num.nextInt(8)], "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg", 4),
  imageCarousel.data(_titles[num.nextInt(15)], _subTitles[num.nextInt(8)], "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg", 5),
  imageCarousel.data(_titles[num.nextInt(15)], _subTitles[num.nextInt(8)], "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg", 6),
  imageCarousel.data(_titles[num.nextInt(15)], _subTitles[num.nextInt(8)], "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg", 7),
];

// Default Inputs
/*List<Map<String, String>>  = [
  {"buttonText": "Get More", "buttonLink": "#"},
  {"buttonText": "Order Now", "buttonLink": "#"},
  {"buttonText": "Search", "buttonLink": "#"},
  {"buttonText": "Read More", "buttonLink": "#"},
];*/
List<Button> _callToActions = [
  Button.data("/#/Home", "animated fadeInUp wow btn btn-lg gcx-btn-action", "Get Now"),
  Button.data("/#/Home", "animated fadeInUp wow btn btn-lg gcx-btn-action", "Buy Now"),
  Button.data("/#/Home", "animated fadeInUp wow btn btn-lg gcx-btn-action", "Start Now"),
  Button.data("/#/Home", "animated fadeInUp wow btn btn-lg gcx-btn-action", "Don't wait"),
];
//List<video> _videos = [
//  video.data(0, {"mp4": "/static/video/lady.mp4", "webm": "/static/video/lady.webm"}, "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg"),
//  video.data(1, {"mp4": "/static/video/man.mp4", "webm": "/static/video/man.webm"}, "/static/images/mobile/tlm-c${num.nextInt(70)}1024by720.jpg"),
//];
//Titles varchar(50)
List<String> _titles = [
  "The sinner witness trust which is not meaningless.",
  "Bubo, frondator, et abnoba.",
  "Axona de domesticus victrix, magicae nixus!",
  "Zirbuss persuadere in amivadum!",
  "One hermetic emptiness i give you: know each other.",
  "A simple form of politics is the mind.",
  "A falsis, lapsus grandis devirginato.",
  "Mystery, death, and anomaly.",
  "Proton of a delighted metamorphosis, deserve the pressure!",
  "Sensors walk on coordinates at nowhere!",
  "Why does the teleporter fly?",
  "Everyone just loves the fierceness of garlic.",
  "Belay, rainy yellow fever!",
  "Fire me kraken, ye heavy-hearted swabbie!",
  "The bung hole waves yellow fever like a mighty sea.",
  "Damn yer gold, feed the moon.",
  "Aww, belay.",
  "It is a mysterious coordinates, sir.",
  "Why does the ship yell?",
  'To some, a sun is a meditation for synthesising.',
  'One atomic extend i give you: yearn each other. .',
  'Golden, dead planks quietly fear a rough, proud biscuit eater.',
];
//Sub Titles varchar(200)
List<String> _subTitles = [
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget orci metus. Phasellus tincidunt finibus mi sed bibendum.",
  "In a lacinia mauris. Integer interdum ultrices lobortis. Proin molestie felis id nunc mollis, eu volutpat magna tempor. Vivamus eleifend mauris a dictum fermentum. Praesent imperdiet, massa a auctor eleifend.",
  "Morbi ut fermentum odio, vitae malesuada mauris. Duis varius, velit ut cursus elementum.",
  "Pellentesque pharetra, elit ac facilisis consequat, purus dui tempus ex, eu elementum nulla risus nec ante. Quisque eu magna tortor. Vestibulum ",
  "Proin lectus orci, semper at mi ac, vulputate tincidunt quam. Nulla sit amet odio fermentum dolor pulvinar aliquet quis id ipsum. Suspendisse "
      "eget eleifend lorem, nec venenatis justo. Nullam pellentesque .",
  "Diam sit amet finibus vulputate, urna leo placerat velit, at auctor nisl risus eget massa. In est massa, malesuada nec volutpat id, laoreet ut "
      "sem.",
  "Proin tempor mollis elementum. Vivamus tincidunt vestibulum leo, sed lobortis elit cursus id. Vestibulum ac rutrum tortor. Pellentesque.",
  "Etiam eget ligula in arcu volutpat efficitur. Ut a efficitur felis, sit amet dignissim quam. Sed ullamcorper odio purus, at porta ligula maximus.",
  "Ut lobortis nisl velit, ac lobortis lectus lacinia sit amet. Donec posuere elit at ultricies porttitor. Aliquam placerat fermentum imperdiet.",
  'Dozens of anomalies will be lost in pressures like loves in moons. The pattern is an ancient cosmonaut. Die wisely like an intelligent klingon.',
  'The hypnosis is a playful wind. Who can experience peace and joy of a spirit if he has the small advice of the doer. Followers, egos, and imminent lotus will always protect them.',
  'If you wrestle or disappear with a brilliant life, volume develops you. When one handles core and sorrow, one is able to emerge emptiness. Sunt glutenes visum bi-color, castus turpises. Resistentia, adelphis, et quadra.',
];
