import 'dart:async';

import 'package:app/logic/event.dart';

import 'main_slide_mock.dart';

class HeaderService {
  Future<List<MainSlideData>> getHeaderMainSlide() async => loadHeaderMainSlide();
}
