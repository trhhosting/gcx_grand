import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:app/logic/event.dart';

import '../../api/main_slide_service.dart';

@Component(
  selector: 'main-slide',
  templateUrl: 'main_slide.html',
  styleUrls: [
    'main_slide.css',
  ],
  directives: [
    coreDirectives,
  ],
  providers: [
    ClassProvider(HeaderService),
  ],
)
class MainSlide implements OnInit, AfterViewChecked {
  List<MainSlideData> data;
  final HeaderService _headerService;
  String carouselActive;
  MainSlide(this._headerService);
  int isMade = 0;
  int count = 0;
  int _countTmp = 1;
  final String t = "#ci-list";
  List<Element> ciList = <Element>[];

  int get countTmp => this._countTmp;
  void next() {
    if (_countTmp >= count || _countTmp == 0) {
      _countTmp = 1;
    } else {
      _countTmp++;
    }
    try {
      int i = 1;
      for (Element c in ciList) {
        if (i == _countTmp) {
          c.setAttribute("class", "active");
        } else {
          c.setAttribute("class", "");
        }
        i++;
      }
    } catch (e) {
      print(e);
    }

    print(_countTmp);
  }

  void prev() {
    if (_countTmp <= 1) {
      _countTmp = ciList.length;
    } else {
      _countTmp--;
    }
    try {
      int i = 1;
      for (Element c in ciList) {
        if (i == _countTmp) {
          c.setAttribute("class", "active");
        } else {
          c.setAttribute("class", "");
        }
        i++;
      }
    } catch (e) {
      print(e);
    }
    print(_countTmp);
  }
  @override
  Future<void> ngOnInit() async {
    data = await _headerService.getHeaderMainSlide();
    count = howManyCarousels(data);
  }
  @override
  void ngAfterViewChecked() {
    if (isMade < 2) {
      ciList = listOfCarouselIndicators(count);
      isMade = isMade + 1;
//      print("Never endure a tobacco.");
    }
  }
}

List<Element> listOfCarouselIndicators(int count) {
  List<Element> ciLists = <Element>[];
  Element ciList = querySelector("#ci-list");
  for (int i = 1; i <= count; i++) {
    Element x = new Element.li();
    x.id = "#ci-${i}";
    ciList.children.add(x);
    ciLists.add(x);
  }

  return ciLists;
}

int addCarouselIndicators(int count) {
  Element ciList = querySelector("#ci-list");
  for (int i = 1; i <= count; i++) {
    Element x = new Element.li();
    x.id = "#ci-${i}";
    ciList.children.add(x);
  }
  return 1;
}

int howManyCarousels(List<MainSlideData> data) {
  int x = 0;
  for (var c in data) {
    for (var ci in c.carousels) {
      x = ci.id;
    }
  }
  return x;
}
