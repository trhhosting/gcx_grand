import 'package:angular/angular.dart';

@Component(
  selector: 'footer-brand',
  templateUrl: 'footer_brand.html',
  styleUrls: [
    'footer_brand.css'
  ],
)
class FooterBrand {
  String copyright = DateTime.now().year.toString();
}
