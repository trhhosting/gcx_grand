REM Navigation, Logo and Slider
ngdart generate component Airisshane --path lib/src/Default/Navigation/airisshane
ngdart generate component Zestia --path lib/src/Default/Logo/zestia
REM Carousel
ngdart generate component Meldin --path lib/src/Default/Slider/meldin
REM All other homepage section and Elements.
REM Such as: About, Services, Portfolio, Counter, Team, Testimonial, Client Logo, Blog, Pricing, and Contact Us
ngdart generate component Wroognygothor --path lib/src/Default/About/Wroognygothor
ngdart generate component Ackmard --path lib/src/Default/Services/Ackmard
ngdart generate component Gametris --path lib/src/Default/Portfolio/Gametris
ngdart generate component Fhuyraslan --path lib/src/Default/Counter/Fhuyraslan
ngdart generate component Zigmal --path lib/src/Default/Team/Zigmal
ngdart generate component Gamud --path lib/src/Default/Testimonial/Gamud
ngdart generate component Aldarenupdar --path lib/src/Default/Client/Aldarenupdar
ngdart generate component Kressara --path lib/src/Default/Logo/Kressara
ngdart generate component Xeniljulthor --path lib/src/Default/Blog/Xeniljulthor
ngdart generate component Hecton --path lib/src/Default/Pricing/Hecton
ngdart generate component Kaldar --path lib/src/Default/ContactUs/Kaldar
ngdart generate component Teressa --path lib/src/Default/p404/Teressa
ngdart generate component DaigornSabalz --path lib/src/Default/p422/DaigornSabalz
ngdart generate component Onathe --path lib/src/Default/p500/Onathe

Rem Components Shared Parts
ngdart generate component Errinaya --path lib/src/Default/shared/Header/Errinaya
ngdart generate component Mehanderyodan --path lib/src/Default/shared/Carousel/Mehanderyodan
ngdart generate component BreenElthen --path lib/src/Default/shared/About/BreenElthen
ngdart generate component Talberonderik --path lib/src/Default/shared/Welcome/Talberonderik
ngdart generate component Othelenyerpal --path lib/src/Default/shared/Services/Othelenyerpal
ngdart generate component ModricNaphazw --path lib/src/Default/shared/Portfolio/ModricNaphazw
ngdart generate component Ahanna --path lib/src/Default/shared/WorkCounter/Ahanna
ngdart generate component Abardonrelban --path lib/src/Default/shared/Team/Abardonrelban
ngdart generate component Senic --path lib/src/Default/shared/Team/Senic
ngdart generate component Akara --path lib/src/Default/shared/Testimonial/Akara
ngdart generate component KassinaTristan --path lib/src/Default/shared/Client/KassinaTristan
ngdart generate component ValtaurGreste --path lib/src/Default/shared/BLog/ValtaurGreste
ngdart generate component Ayne --path lib/src/Default/shared/ContactUs/Ayne
ngdart generate component Teressa --path lib/src/Default/shared/Action/Teressa
ngdart generate component Laenaya --path lib/src/Default/shared/Accirdion/Laenaya
ngdart generate component Luna --path lib/src/Default/shared/Alert/Luna
ngdart generate component Urda --path lib/src/Default/shared/Pricing/Urda
ngdart generate component Rahe --path lib/src/Default/shared/Pricing/Rahe
ngdart generate component Irina --path lib/src/Default/shared/Pricing/Irina
ngdart generate component Useli --path lib/src/Default/shared/Tabs/Useli
ngdart generate component Qupar --path lib/src/Default/shared/Tabs/Qupar
ngdart generate component Vilan --path lib/src/Default/shared/Tabs/Vilan


REM Pages
ngdart generate component About --path lib/src/Default/pages/About
ngdart generate component AddProduct --path lib/src/Default/pages/AddProduct
ngdart generate component BlogPost --path lib/src/Default/pages/BlogPost
ngdart generate component BlogPosts --path lib/src/Default/pages/BlogPosts
ngdart generate component ContactUs --path lib/src/Default/pages/ContactUs
ngdart generate component Discover --path lib/src/Default/pages/Discover
ngdart generate component Ecommerce --path lib/src/Default/pages/Ecommerce
ngdart generate component Home --path lib/src/Default/pages/Home
ngdart generate component Landing --path lib/src/Default/pages/Landing
ngdart generate component Licenses --path lib/src/Default/pages/Licenses
ngdart generate component Login --path lib/src/Default/pages/Login
ngdart generate component P404 --path lib/src/Default/pages/P404
ngdart generate component P422 --path lib/src/Default/pages/P422
ngdart generate component P500 --path lib/src/Default/pages/P500
ngdart generate component Product --path lib/src/Default/pages/Product
ngdart generate component Profile --path lib/src/Default/pages/Profile
ngdart generate component Register --path lib/src/Default/pages/Register
ngdart generate component SearchSidebar --path lib/src/Default/pages/SearchSidebar
ngdart generate component Settings --path lib/src/Default/pages/Settings
ngdart generate component TeamInfo --path lib/src/Default/pages/TeamInfo